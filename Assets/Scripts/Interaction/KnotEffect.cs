﻿using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.Interaction
{
    public class KnotEffect
    {
        public StatEffect positiveEffect { get; private set; }
        public StatEffect negativeEffect { get; private set; }

        StatIdentifier[] stats =
        {
            StatIdentifier.DoubleFruitChance,
            StatIdentifier.FruitQuality,
            StatIdentifier.FruitGrowSpeed,
            StatIdentifier.Thirst,
            StatIdentifier.TreeGrowSpeed,
            StatIdentifier.TreeGrowSpeed
        };

        public KnotEffect()
        {
            var firstStat = stats.PickOne();
            var secondStat = stats.PickOneExcept(firstStat);

            positiveEffect = new StatEffect(firstStat, Random.Range(2f, 9f));
            negativeEffect = new StatEffect(secondStat, -Random.Range(3f, 8f));
        }
    }
}

﻿using Assets.Scripts.Vegetation;
using UnityEngine;

namespace Assets.Scripts.Interaction
{
    public class FruitInteractor : MonoBehaviour
    {
        [SerializeField] SpriteRenderer highlightRing;
        Fruit fruit;
        public bool CanHarvest => fruit.Age > 0.5f;

        public void Initialize(Fruit fruit)
        {
            Hide();
            this.fruit = fruit;
        }

        public void Harvest() => 
            fruit.Harvest();

        public void Highlight() => highlightRing.enabled = CanHarvest;
        public void Hide() => highlightRing.enabled = false;
    }
}
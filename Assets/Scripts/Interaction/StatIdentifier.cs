﻿namespace Assets.Scripts.Interaction
{
    public enum StatIdentifier
    {
        TreeGrowSpeed,
        FruitGrowSpeed,
        FruitQuality,
        Thirst,
        DoubleFruitChance
    }
}
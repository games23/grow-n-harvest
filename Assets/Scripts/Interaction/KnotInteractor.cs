﻿using Assets.Scripts.Vegetation;
using UnityEngine;

namespace Assets.Scripts.Interaction
{
    public class KnotInteractor : MonoBehaviour
    {
        [SerializeField] SpriteRenderer highlightRing;

        Knot knot;
        KnotEffect effect;

        public bool CanCut => knot.CanCut();

        public void Initialize(Knot knot)
        {
            Hide();
            this.knot = knot;
            effect = new KnotEffect();
        }

        public KnotEffect GetInfo() => effect;

        public void Highlight() => highlightRing.enabled = true;
        public void Hide() => highlightRing.enabled = false;

        public void Prune()
        {
            knot.Prune(effect);
            effect = new KnotEffect();
        }
    }
}
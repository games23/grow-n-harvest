﻿using UnityEngine;
using Tree = Assets.Scripts.Vegetation.Tree;

namespace Assets.Scripts.Interaction
{
    public class TreeInteractor : MonoBehaviour
    {
        [SerializeField] Tree tree;

        public void Water(float amount) => 
            tree.AddWater(amount);
    }
}
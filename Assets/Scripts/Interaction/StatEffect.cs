﻿namespace Assets.Scripts.Interaction
{
    public struct StatEffect
    {
        public StatIdentifier Stat { get; private set; }
        public float Amount { get; private set; }

        public StatEffect(StatIdentifier stat, float amount)
        {
            Stat = stat;
            Amount = amount;
        }

        public override string ToString() => 
            Amount > 0 ? $"+{Amount:0}% {StatText(Stat)}" : $"{Amount:0}% {StatText(Stat)}";

        string StatText(StatIdentifier stat)
        {
            switch (stat)
            {
                case StatIdentifier.DoubleFruitChance:
                    return "to double fruit chance";
                case StatIdentifier.FruitGrowSpeed:
                    return "fruit grow speed";
                case StatIdentifier.FruitQuality:
                    return "fruit quality";
                case StatIdentifier.Thirst:
                    return "water retention";
                case StatIdentifier.TreeGrowSpeed:
                    return "tree grow speed";
            }

            return string.Empty;
        }
    }
}
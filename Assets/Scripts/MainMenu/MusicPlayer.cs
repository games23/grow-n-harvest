﻿using UnityEngine;

namespace Assets.Scripts.MainMenu
{
    public class MusicPlayer : MonoBehaviour
    {
        void Awake()
        {
            var players = GameObject.FindGameObjectsWithTag("music");

            if (players.Length > 1)
                Destroy(this.gameObject);

            DontDestroyOnLoad(this.gameObject);
        }
    }
}

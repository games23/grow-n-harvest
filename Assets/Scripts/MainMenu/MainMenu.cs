﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.MainMenu
{
    public class MainMenu : MonoBehaviour
    {
        public void NewGame() => 
            SceneManager.LoadScene("Garden");

        public void Exit() =>
            Application.Quit();
        
    }
}

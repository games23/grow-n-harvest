﻿using Assets.Scripts.Interaction;
using Assets.Scripts.Player;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class Fruit : MonoBehaviour
    {
        [SerializeField] Gradient growthColor;
        [SerializeField] float maxScale;
        [SerializeField] SpriteRenderer sprite;
        [SerializeField] Transform growthNode;
        [SerializeField] FruitInteractor interactor;

        float growTime;
        float elapsed = 0f;
        TreeStats stats;
        Tree tree;
        float quality;

        public float Age { get; private set; }

        public void Initialize(TreeStats stats, Tree tree)
        {
            this.tree = tree;
            this.stats = stats;
            quality = stats.fruitQuality;

            interactor.Initialize(this);
            growTime = stats.branchGrowthTime +
                       Random.Range(stats.branchGrowthTime * -0.2f, stats.branchGrowthTime * 0.2f);
            Age = 0f;
            enabled = true;
            elapsed = 0f;
        }

        void Update()
        {
            if (Age < 1)
            {
                elapsed += Time.deltaTime * tree.GrowMultiplier;
                Age = elapsed / growTime;
                growthNode.localScale = Vector3.one * Age * maxScale;
                sprite.color = growthColor.Evaluate(Age);
            }
            else
                enabled = false;
        }

        public void Harvest()
        {
            tree.OnFruitHarvested(Mathf.RoundToInt(stats.fruitBaseValue * quality * Age));
            Initialize(stats, tree);
        }
    }
}

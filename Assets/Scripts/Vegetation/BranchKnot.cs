﻿using System.Linq;
using Assets.Scripts.Vegetation.DataStructures;

namespace Assets.Scripts.Vegetation
{
    public class BranchKnot : Knot
    {
        public override void Initialize(int order, TreeStats stats, Tree tree)
        {
            base.Initialize(order, stats, tree);
            BranchOut();
        }

        void BranchOut()
        {
            foreach (var _ in Enumerable.Range(1, stats.BranchVolume))
            {
                var segment = Instantiate(stats.branchSegmentPrefab, transform);
                segment.Initialize(order, stats, tree);
                attachedSegments.Add(segment.gameObject);
            }
        }
    }
}

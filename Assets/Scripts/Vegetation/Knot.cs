﻿using System.Collections.Generic;
using Assets.Scripts.Interaction;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public abstract class Knot : MonoBehaviour
    {
        [SerializeField] protected KnotInteractor interactor;

        protected TreeStats stats;
        protected int order;

        protected List<GameObject> attachedSegments;
        protected Tree tree;

        public virtual void Initialize(int order, TreeStats stats, Tree tree)
        {
            this.tree = tree;
            attachedSegments = new List<GameObject>();
            interactor.Initialize(this);
            this.stats = stats;
            this.order = order;
        }

        public bool CanCut() =>
            true;

        public void Prune(KnotEffect effect)
        {
            foreach (var segment in attachedSegments)
                Destroy(segment);

            tree.ApplyEffect(effect.positiveEffect);
            tree.ApplyEffect(effect.negativeEffect);

            Initialize(order, stats, tree);
        }
    }
}

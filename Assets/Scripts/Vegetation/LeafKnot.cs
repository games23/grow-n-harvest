﻿using System.Linq;
using Assets.Scripts.Interaction;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class LeafKnot : Knot
    {
        [SerializeField] SpriteRenderer leafs;
        [SerializeField] float maxScale;

        float age = 0;
        float elapsed = 0f;

        public override void Initialize(int order, TreeStats stats, Tree tree)
        {
            base.Initialize(order, stats, tree);

            leafs.transform.rotation = Quaternion.identity;
            BranchOut();
        }

        void BranchOut()
        {
            var branches = Random.value <= stats.doubleFruitChance ? 2 : 1;

            foreach (var _ in Enumerable.Range(1, branches))
            {
                var segment = Instantiate(stats.branchSegmentPrefab, transform);
                segment.Initialize(order + 1, stats, tree);
                attachedSegments.Add(segment.gameObject);
            }
        }

        void Update()
        {
            if (age < 1)
            {
                elapsed += Time.deltaTime * tree.GrowMultiplier;
                age = elapsed / stats.branchGrowthTime;
                leafs.transform.localScale = Vector3.one * age * maxScale;
            }
            else
            {
                enabled = false;
            }
        }
    }
}

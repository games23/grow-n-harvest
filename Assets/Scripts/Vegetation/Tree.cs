using Assets.Scripts.Interaction;
using Assets.Scripts.Player;
using Assets.Scripts.UI;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class Tree : MonoBehaviour
    {
        [SerializeField] TreeStats baseStats;
        [SerializeField] TrunkSegment seedTrunk;
        [SerializeField] Sprite fruitIcon;
        [SerializeField] Color fruitColor;

        TreeStatusUI statusUI;
        TreeStats currentStats;


        float optimalWater = 100f;
        float currentWater = 50f;
        DeliveryTracker deliveryTracker;

        public float GrowMultiplier { get; private set; }

        public void Initialize(TreeStatusUI statusPanel, DeliveryTracker deliveryTracker)
        {
            this.statusUI = statusPanel;
            statusUI.SetUpIcon(fruitIcon, fruitColor);
            this.deliveryTracker = deliveryTracker;

            currentStats = baseStats.CopyTo();

            var trunk = Instantiate(seedTrunk, transform);
            trunk.Initialize(1, currentStats, this);
        }

        public void ApplyEffect(StatEffect effect)
        {
            switch (effect.Stat)
            {
                case StatIdentifier.DoubleFruitChance:
                    currentStats.doubleFruitChance += currentStats.doubleFruitChance * effect.Amount / 100f;
                    break;
                case StatIdentifier.FruitGrowSpeed:
                    currentStats.fruitGrowthTime -= currentStats.fruitGrowthTime * effect.Amount / 100f;
                    break;
                case StatIdentifier.FruitQuality:
                    currentStats.fruitQuality += currentStats.fruitQuality * effect.Amount*5f / 100f;
                    if (currentStats.fruitQuality < 1)
                        currentStats.fruitQuality = 1;
                    statusUI.UpdateQuality(Mathf.FloorToInt(currentStats.fruitQuality));
                    break;
                case StatIdentifier.Thirst:
                    currentStats.thirst -= currentStats.thirst * effect.Amount / 100f;
                    break;
                case StatIdentifier.TreeGrowSpeed:
                    currentStats.branchGrowthTime -= currentStats.branchGrowthTime * effect.Amount / 100f;
                    currentStats.trunkGrowthTime -= currentStats.trunkGrowthTime * effect.Amount / 100f;
                    break;
            }
        }

        void Update()
        {
            currentWater -= currentStats.thirst;
            if (currentWater <= 0)
                currentWater = 0;
            if (currentWater <= optimalWater)
                GrowMultiplier = currentWater / optimalWater;
            else
                GrowMultiplier = (optimalWater - (currentWater - optimalWater)) / optimalWater;

            statusUI.UpdateWater(currentWater);
        }

        public void AddWater(float amount)
        {
            currentWater += amount;
            if (currentWater > optimalWater * 2)
                currentWater = optimalWater * 2;
        }

        public void OnFruitHarvested(int totalValue) => 
            deliveryTracker.OnFruitHarvested(totalValue);
    }
}

﻿using System.Linq;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class BranchSegment : MonoBehaviour
    {
        [SerializeField] LineRenderer line;
        [SerializeField] float deviation;

        TreeStats stats;

        float maxLength;
        float width;

        float growTime;
        float age = 0f;
        float elapsed = 0f;
        int order;
        Tree tree;

        public void Initialize(int order, TreeStats stats, Tree tree)
        {
            this.tree = tree;
            this.stats = stats;
            this.order = order;

            growTime = stats.branchGrowthTime +
                       Random.Range(stats.branchGrowthTime * -0.2f, stats.branchGrowthTime * 0.2f);
            maxLength = stats.branchLength / (order * 0.2f);
            width = stats.branchWidth / (order * 2f);
            deviation += order * 2;

            float angle = Random.Range(-deviation, deviation);
            transform.Rotate(Vector3.forward, angle);
        }

        public void InitializeAsTrunkSide(int order, TreeStats stats, Tree tree)
        {
            Initialize(order,stats, tree);

            transform.localRotation = Quaternion.identity;
            deviation = Random.value > 0.5f ? Random.Range(70f, 95f) : -Random.Range(70f, 95f);
            transform.Rotate(Vector3.forward, deviation);
        }

        void Update()
        {
            if (age < 1)
            {
                elapsed += Time.deltaTime * tree.GrowMultiplier;
                age = elapsed / growTime;
                var segmentEnd = Vector3.Lerp(transform.position, transform.position + (transform.up * maxLength), age);

                line.SetPositions(new[] { transform.position, segmentEnd });
                line.widthCurve = new AnimationCurve(new Keyframe(0, age * width), new Keyframe(1, age * width * 0.5f));
            }
            else
            {
                BranchOut();
                enabled = false;
            }
        }

        void BranchOut()
        {
            if (order < stats.FruitOrder - 1)
            {
                var knot = Instantiate(stats.branchKnotPrefab, transform.position + (transform.up * maxLength),
                    transform.rotation);
                knot.transform.SetParent(transform);
                knot.Initialize(order + 1, stats, tree);
            }
            else if (order == stats.FruitOrder - 1)
            {
                var knot = Instantiate(stats.leafKnotPrefab, transform.position + (transform.up * maxLength),
                    transform.rotation);
                knot.transform.SetParent(transform);
                knot.Initialize(order + 1, stats, tree);
            }
            else
            {
                var fruit = Instantiate(stats.fruitPrefab, transform);
                fruit.transform.position = transform.position + (transform.up * maxLength);
                fruit.Initialize(stats, tree);
            }
        }
    }
}

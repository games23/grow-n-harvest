﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Interaction;
using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class TrunkKnot : Knot
    {
        public override void Initialize(int order, TreeStats stats, Tree tree)
        {
            base.Initialize(order, stats, tree);

            TrunkOut();
            BranchOut();
        }

        void BranchOut()
        {
            if (Random.value <= stats.trunkBranchChance)
            {
                var branch = Instantiate(stats.branchSegmentPrefab, transform);
                branch.InitializeAsTrunkSide(order + 3, stats, tree);
                attachedSegments.Add(branch.gameObject);
            }
        }

        void TrunkOut()
        {
            var trunk = Instantiate(stats.trunkSegmentPrefab, transform);
            trunk.Initialize(order, stats, tree);
            attachedSegments.Add(trunk.gameObject);
        }
    }
}

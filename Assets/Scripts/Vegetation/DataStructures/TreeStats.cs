﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Vegetation.DataStructures
{
    [CreateAssetMenu(menuName = "Configuration/Tree Stats", fileName = "BaseTreeStats")]
    public class TreeStats : ScriptableObject
    {
        public TrunkSegment trunkSegmentPrefab;
        public BranchSegment branchSegmentPrefab;
        public TrunkKnot trunkKnotPrefab;
        public BranchKnot branchKnotPrefab;
        public LeafKnot leafKnotPrefab;
        public Fruit fruitPrefab;

        public int MaxTrunkOrder;
        public int FruitOrder;
        public int BranchVolume;

        public float trunkBranchChance;
        public float doubleFruitChance;

        public float trunkLength;
        public float branchLength;
        public float trunkWidth;
        public float branchWidth;
        public float trunkGrowthTime;
        public float branchGrowthTime;
        public float fruitGrowthTime;
        public float fruitQuality;
        public float thirst;
        public int fruitBaseValue;

        public TreeStats CopyTo()
        {
            return new TreeStats()
            {
                trunkSegmentPrefab = this.trunkSegmentPrefab,
                branchSegmentPrefab = this.branchSegmentPrefab,
                trunkKnotPrefab = this.trunkKnotPrefab,
                branchKnotPrefab = this.branchKnotPrefab,
                leafKnotPrefab = this.leafKnotPrefab,
                fruitPrefab = this.fruitPrefab,
                MaxTrunkOrder = this.MaxTrunkOrder,
                FruitOrder = this.FruitOrder,
                BranchVolume = this.BranchVolume,
                trunkBranchChance = this.trunkBranchChance,
                doubleFruitChance = this.doubleFruitChance,
                trunkLength = this.trunkLength,
                branchLength = this.branchLength,
                trunkWidth = this.trunkWidth,
                branchWidth = this.branchWidth,
                trunkGrowthTime = this.trunkGrowthTime,
                branchGrowthTime = this.branchGrowthTime,
                fruitGrowthTime = this.fruitGrowthTime,
                fruitQuality = this.fruitQuality,
                thirst = this.thirst,
                fruitBaseValue = this.fruitBaseValue
            };
        }
    }
}

﻿using Assets.Scripts.Vegetation.DataStructures;
using UnityEngine;

namespace Assets.Scripts.Vegetation
{
    public class TrunkSegment : MonoBehaviour
    {
        [SerializeField] LineRenderer line;
        [SerializeField] float deviation;

        TreeStats stats;

        float maxLength;
        float width;

        float growTime;
        float age = 0f;
        float elapsed = 0f;
        int order;
        Tree tree;

        public void Initialize(int order, TreeStats stats, Tree tree)
        {
            this.tree = tree;
            this.stats = stats;
            this.order = order;

            growTime = stats.branchGrowthTime +
                       Random.Range(stats.branchGrowthTime * -0.2f, stats.branchGrowthTime * 0.2f);
            maxLength = stats.trunkLength / (order * 0.5f);
            width = stats.trunkWidth / (order * 2f);
            deviation += order*2;

            float angle = Random.Range(-deviation, deviation);
            transform.Rotate(Vector3.forward, angle);
        }

        void Update()
        {
            if (age < 1)
            {
                elapsed += Time.deltaTime * tree.GrowMultiplier;
                age = elapsed / growTime;
                var segmentEnd = Vector3.Lerp(transform.position, transform.position + (transform.up * maxLength), age);

                line.SetPositions(new[] {transform.position, segmentEnd});
                line.widthCurve = new AnimationCurve(new Keyframe(0, age * width), new Keyframe(1, age * width*0.6f));
            }
            else
            {
                TrunkOut();
                enabled = false;
            }
        }

        void TrunkOut()
        {
            if (order < stats.MaxTrunkOrder)
            {
                var knot = Instantiate(stats.trunkKnotPrefab, transform.position + (transform.up * maxLength),
                    transform.rotation);
                knot.transform.SetParent(transform);
                knot.Initialize(order + 1, stats, tree);
            }
            else
            {
                var knot = Instantiate(stats.branchKnotPrefab, transform.position + (transform.up * maxLength),
                    transform.rotation);
                knot.transform.SetParent(transform);
                knot.Initialize(order + 1, stats, tree);
            }
        }


    }
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class ExtensionMethods
    {
        public static T PickOne<T>(this IEnumerable<T> source) => 
            source.ToArray()[Random.Range(0, source.Count() - 1)];

        public static T PickOneExcept<T>(this IEnumerable<T> source, T exception)
        {
            var candidates = source.Except(new T[] {exception});
            return candidates.ToArray()[Random.Range(0, candidates.Count() - 1)];
        }

        public static IEnumerable<T> PickAmount<T>(this IEnumerable<T> source, int amount)
        {
            var p =  Random.Range(0, source.Count() - 1);
            return source.OrderBy(_ => p).Take(amount);
        }
    }
}

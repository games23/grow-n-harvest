﻿using Assets.Scripts.Player;
using Assets.Scripts.UI;
using UnityEngine;
using Tree = Assets.Scripts.Vegetation.Tree;


namespace Assets.Scripts.Gardening
{
    public class StartingTreePot : MonoBehaviour
    {
        [SerializeField] DeliveryTracker deliveryTracker;
        [SerializeField] Tree startingTree;
        [SerializeField] TreeStatusUI statusUi;

        void Start() => 
            startingTree.Initialize(statusUi, deliveryTracker);
    }
}

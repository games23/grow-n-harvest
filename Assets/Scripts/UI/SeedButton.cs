﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class SeedButton : MonoBehaviour
    {
        [SerializeField] Vegetation.Tree seedPrefab;
        [SerializeField] Button button;
        [SerializeField] int price;
        
        public void Initialize(Action<int, Vegetation.Tree> onClick) => 
            button.onClick.AddListener(() => onClick(price, seedPrefab));
    }
}

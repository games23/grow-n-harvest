﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class StarQualityPanel : MonoBehaviour
    {
        [SerializeField] Image star1;
        [SerializeField] Image star2;
        [SerializeField] Image star3;

        [SerializeField] Color emptyColor;
        [SerializeField] Color fullColor;


        public void SetQuality(int quality)
        {
            star1.color = quality >= 1 ? fullColor : emptyColor;
            star2.color = quality >= 2 ? fullColor : emptyColor;
            star3.color = quality >= 3 ? fullColor : emptyColor;
        }
    }
}

﻿using Assets.Scripts.Interaction;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class KnotTooltip : MonoBehaviour
    {
        [SerializeField] TMP_Text positiveEffect;
        [SerializeField] TMP_Text negativeEffect;
        [SerializeField] GameObject panel;

        public void Show(KnotEffect effect)
        {
            transform.position = Input.mousePosition;
            panel.SetActive(true);
            positiveEffect.text = effect.positiveEffect.ToString();
            negativeEffect.text = effect.negativeEffect.ToString();
        }

        public void Hide() => 
            panel.SetActive(false);
    }
}
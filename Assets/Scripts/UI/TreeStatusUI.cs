using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class TreeStatusUI : MonoBehaviour
    {
        [SerializeField] Image normalFill;
        [SerializeField] Image drownFill;
        [SerializeField] StarQualityPanel qualityPanel;
        [SerializeField] Image fruitIcon;

        float maxFill = 100f;

        public void SetUpIcon(Sprite fruitSprite, Color color)
        {
            fruitIcon.sprite = fruitSprite;
            fruitIcon.color = color;
        }

        public void UpdateWater(float currentWater)
        {
            normalFill.fillAmount = currentWater / maxFill;
            drownFill.fillAmount = (currentWater - maxFill) / maxFill;
        }

        public void UpdateQuality(int quality) => 
            qualityPanel.SetQuality(quality);
    }
}
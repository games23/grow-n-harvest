﻿using Assets.Scripts.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class DeliveryPanel : MonoBehaviour
    {
        [SerializeField] TMP_Text progressReport;
        [SerializeField] Image timer;
        [SerializeField] TMP_Text funds;

        public void UpdateRemainingTime(float time, float elapsed) => 
            timer.fillAmount = 1 - (elapsed / time);

        public void UpdatePanel(DeliveryGoal goal) => 
            progressReport.text = $"{goal.Progress} / {goal.Value}";

        public void UpdateFunds(int amount) => 
            funds.text = $"${amount}";
    }
}

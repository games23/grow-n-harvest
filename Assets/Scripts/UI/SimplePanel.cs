﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class SimplePanel : MonoBehaviour
    {
        public void Close() => 
            gameObject.SetActive(false);
    }
}

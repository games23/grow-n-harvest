﻿using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ShopPanel : MonoBehaviour
    {
        [SerializeField] Transform seedSpot;
        [SerializeField] TreeStatusUI statusPanel;
        [SerializeField] DeliveryTracker deliveryTracker;

        [SerializeField] SeedButton[] seedButtons;

        void Start()
        {
            foreach (var sb in seedButtons)
                sb.Initialize(OnButtonClick);
        }

        void OnButtonClick(int price, Vegetation.Tree seedPrefab)
        {
            if (deliveryTracker.Funds >= price)
            {
                deliveryTracker.OnTreePurchase(price);
                var tree = Instantiate(seedPrefab, seedSpot);
                tree.Initialize(statusPanel, deliveryTracker);

                statusPanel.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }
}

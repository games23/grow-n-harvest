﻿using Assets.Scripts.Interaction;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class HarvestToolControl : MonoBehaviour
    {
        [SerializeField] LayerMask fruitLayer;
        [SerializeField] AudioSource harvestSfx;
        FruitInteractor lastInteractor;

        void Update()
        {
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit, 20f, fruitLayer))
            {
                lastInteractor = hit.collider.GetComponent<FruitInteractor>();
                lastInteractor.Highlight();

                if (Input.GetMouseButtonDown(0) && lastInteractor.CanHarvest)
                {
                    harvestSfx.Play();
                    lastInteractor.Harvest();
                }
            }
            else
            {
                lastInteractor?.Hide();
            }
        }
    }
}

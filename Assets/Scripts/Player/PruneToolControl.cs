﻿using Assets.Scripts.Interaction;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PruneToolControl : MonoBehaviour
    {
        [SerializeField] LayerMask knotLayer;
        [SerializeField] KnotTooltip knotTooltip;
        [SerializeField] AudioSource pruneSfx;

        KnotInteractor lastInteractor;

        void Update()
        {
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit, 20f, knotLayer))
            {
                lastInteractor = hit.collider.GetComponent<KnotInteractor>();
                lastInteractor.Highlight();
                knotTooltip.Show(lastInteractor.GetInfo());

                if (Input.GetMouseButtonDown(0) && lastInteractor.CanCut)
                {
                    pruneSfx.Play();
                    lastInteractor.Prune();
                }
            }
            else
            {
                knotTooltip.Hide();
                lastInteractor?.Hide();
            }
        }
    }
}

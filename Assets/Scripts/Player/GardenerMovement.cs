﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class GardenerMovement : MonoBehaviour
    {
        [SerializeField] Transform[] walkSpots;
        [SerializeField] float speed;

        Transform currentSpot;
        
        void Start() =>
            currentSpot = walkSpots.First();

        void Update() => 
            transform.position = Vector3.Lerp(transform.position, currentSpot.position, speed);

        public void MoveRight()
        {
            var currentIndex = walkSpots.ToList().IndexOf(currentSpot);
            var index = currentIndex + 1;
            if (index < walkSpots.Length)
                currentSpot = walkSpots[index];
        }

        public void MoveLeft()
        {
            var currentIndex = walkSpots.ToList().IndexOf(currentSpot);
            var index = currentIndex - 1;
            if (index >= 0)
                currentSpot = walkSpots[index];
        }
    }
}

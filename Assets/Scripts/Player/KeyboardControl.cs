﻿using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Player
{
    public class KeyboardControl : MonoBehaviour
    {
        [SerializeField] PruneToolControl prune;
        [SerializeField] WaterToolControl water;
        [SerializeField] HarvestToolControl harvest;

        [SerializeField] GameObject pruneGun;
        [SerializeField] GameObject waterGun;
        [SerializeField] GameObject harvestGun;

        [SerializeField] Button pruneButton;
        [SerializeField] Button waterButton;
        [SerializeField] Button harvestButton;

        [SerializeField] GardenerMovement movement;
        [SerializeField] KnotTooltip tooltip;

        [SerializeField] AudioSource changeToolSfx;

        void Start()
        {
            DisableTools();
            pruneButton.onClick.AddListener(SelectPrune);
            waterButton.onClick.AddListener(SelectWater);
            harvestButton.onClick.AddListener(SelectHarvest);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                SelectPrune();
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                SelectWater();
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                SelectHarvest();

            if (Input.GetKeyDown(KeyCode.A))
                movement.MoveLeft();
            else if (Input.GetKeyDown(KeyCode.D))
                movement.MoveRight();

            if (Input.GetMouseButtonDown(1))
                DisableTools();

            if (Input.GetKeyDown(KeyCode.Escape))
                SceneManager.LoadScene("Menu");
        }

        public void SelectPrune()
        {
            DisableTools();
            prune.enabled = true;
            pruneGun.SetActive(true);
            pruneButton.transform.localScale = Vector3.one;
            changeToolSfx.Play();
        }

        public void SelectWater()
        {
            DisableTools();
            water.enabled = true;
            waterGun.SetActive(true);
            waterButton.transform.localScale = Vector3.one;
            changeToolSfx.Play();
        }

        public void SelectHarvest()
        {
            DisableTools();
            harvest.enabled = true;
            harvestGun.SetActive(true);
            harvestButton.transform.localScale = Vector3.one;
            changeToolSfx.Play();
        }

        void DisableTools()
        {
            tooltip.Hide();
            prune.enabled = false;
            water.enabled = false;
            harvest.enabled = false;

            pruneGun.SetActive(false);
            waterGun.SetActive(false);
            harvestGun.SetActive(false);

            pruneButton.transform.localScale = Vector3.one * 0.7f;
            waterButton.transform.localScale = Vector3.one * 0.7f;
            harvestButton.transform.localScale = Vector3.one * 0.7f;
        }

    }
}

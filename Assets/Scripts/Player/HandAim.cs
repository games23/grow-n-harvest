﻿using UnityEngine;

namespace Assets.Scripts.Player
{
    public class HandAim : MonoBehaviour
    {
        [SerializeField] LayerMask wallLayer;
        [SerializeField] Transform hand;

        void Update()
        {
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit, 20f, wallLayer))
            {
                hand.LookAt(hit.point);
                var euler = hand.eulerAngles;
                euler.z = 0f;
                hand.eulerAngles = euler;
            }
        }
    }
}

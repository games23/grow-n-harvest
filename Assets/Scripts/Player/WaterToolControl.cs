﻿using Assets.Scripts.Interaction;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class WaterToolControl : MonoBehaviour
    {
        [SerializeField] LayerMask treeLayer;
        [SerializeField] float waterPerFrame;
        [SerializeField] ParticleSystem waterVfx;
        [SerializeField] AudioSource waterSfx;

        void Update()
        {
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit, 20f, treeLayer))
            {
                var interactor = hit.collider.GetComponent<TreeInteractor>();
                if (Input.GetMouseButton(0))
                {
                    interactor.Water(waterPerFrame);
                    waterVfx.Emit(1);
                    if(!waterSfx.isPlaying)
                        waterSfx.Play();
                }
            }
        }
    }
}

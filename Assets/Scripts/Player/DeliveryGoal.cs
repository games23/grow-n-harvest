﻿namespace Assets.Scripts.Player
{
    public class DeliveryGoal
    {
        public int Value { get;}
        public int Progress { get; private set; }

        public float Time { get; }
        public bool Completed => Progress >= Value;

        public DeliveryGoal(int value, float time)
        {
            Value = value;
            Time = time;
            Progress = 0;
        }

        public void AddProgress(int value) => Progress += value;
    }
}
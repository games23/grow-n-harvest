﻿using Assets.Scripts.UI;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class DeliveryTracker : MonoBehaviour
    {
        [SerializeField] int initialValue;
        [SerializeField] float initialTime;
        [SerializeField] float rewardRatio;
        [SerializeField] float valueIncrementPerGoal;
        [SerializeField] float timeIncrementPerGoal;
        [SerializeField] DeliveryPanel uiPanel;
        [SerializeField] AudioSource deliverySfx;
        [SerializeField] AudioSource newTreeSfx;

        DeliveryGoal currentDeliveryGoal;
        float elapsedTime;

        public int Funds { get; private set; }

        void Start()
        {
            currentDeliveryGoal = new DeliveryGoal(initialValue, initialTime);
            elapsedTime = 0;
            uiPanel.UpdatePanel(currentDeliveryGoal);
            uiPanel.UpdateFunds(Funds);
        }

        void SetNewGoal()
        {
            var nextValue = currentDeliveryGoal.Value + Mathf.RoundToInt(currentDeliveryGoal.Value * valueIncrementPerGoal);
            var nextTime = currentDeliveryGoal.Time + currentDeliveryGoal.Time * timeIncrementPerGoal;

            currentDeliveryGoal = new DeliveryGoal(nextValue, nextTime);
            elapsedTime = 0;
            uiPanel.UpdatePanel(currentDeliveryGoal);
        }

        public void OnFruitHarvested(int value)
        {
            currentDeliveryGoal.AddProgress(value);

            if (currentDeliveryGoal.Completed)
            {
                Funds += Mathf.RoundToInt(currentDeliveryGoal.Value * rewardRatio);
                uiPanel.UpdateFunds(Funds);
                deliverySfx.Play();
                SetNewGoal();
            }

            uiPanel.UpdatePanel(currentDeliveryGoal);
        }

        void Update()
        {
            elapsedTime += Time.deltaTime;
            uiPanel.UpdateRemainingTime(currentDeliveryGoal.Time, elapsedTime);
        }

        public void OnTreePurchase(int seedValue)
        {
            newTreeSfx.Play();
            Funds -= seedValue;
            uiPanel.UpdateFunds(Funds);
        }
    }
}
